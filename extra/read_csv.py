import csv

def read_csv(filename):
    # dict_cat_img is a dictionary of format: dict[cat] = [img_url1, img_url2, ... ]
    dict_cat_img = {}
    # dict_img_limit is a dictionary of format: dict[img_url] = limit_show_amout
    dict_img_limit = {}
    with open(filename) as csvfile:
        config_reader = csv.reader(csvfile)
        for note in config_reader:
            try:
                list_note = note[0].split(';')
                img_url, prepaid_show_amount, categories = list_note[0], list_note[1], list_note[2:]
            except:
                continue
            for category in categories:
                if category not in dict_cat_img:
                    dict_cat_img[category] = []
                if img_url not in dict_cat_img[category]:
                    dict_cat_img[category].append(img_url)
            if img_url not in dict_img_limit:
                dict_img_limit[img_url] = prepaid_show_amount
    return dict_cat_img, dict_img_limit