import os
test_dir = os.path.abspath(os.path.dirname(__file__))
app_dir = os.path.abspath(os.path.join(test_dir, os.pardir))
import sys
sys.path.append(app_dir)
import unittest

from app import models, db, app
from extra.read_csv import read_csv

class TestCase(unittest.TestCase):

    def setUp(self):
        #Another database for testing
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(test_dir, 'test.db')
        #Create database if needed
        db.create_all()
        #Read test CSV configuration file
        self.dict_cat_img, self.dict_img_limit = read_csv(os.path.join(test_dir, 'test_config.csv'))
        #Clear old data in Database
        models.Img_limit.query.delete()
        #Insert data parsed from CSV config file
        for img in self.dict_img_limit:
            db.session.add(models.Img_limit(img_url = img, limit = self.dict_img_limit[img]))
        db.session.commit()

    def test_get_banner_without_cat(self):
        suitable_imgs = self.dict_img_limit.keys()

        session = {}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == 'http://banners.com/banner2.jpg'

    def test_get_banner_from_the_only_cat_and_when_imgs_not_available(self):
        categories = ['cat1']
        suitable_imgs = suitable_imgs = set(sum([self.dict_cat_img[cat] for cat in categories if cat in self.dict_cat_img],[]))

        session = {}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == 'http://banners.com/banner1.jpg'

        session = {'current_banner' : 'http://banners.com/banner1.jpg'}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == ''

    def test_get_banner_two_cat_and_when_imgs_not_available(self):
        self.get_banner_some_cat_and_when_imgs_not_available(['cat1','cat2'])

    def test_get_banner_common_cat_and_when_imgs_not_available(self):
        self.get_banner_some_cat_and_when_imgs_not_available(['cat3'])

    def get_banner_some_cat_and_when_imgs_not_available(self,categories):
        suitable_imgs = set(sum([self.dict_cat_img[cat] for cat in categories if cat in self.dict_cat_img],[]))

        session = {}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == 'http://banners.com/banner2.jpg'

        session = {'current_banner' : 'http://banners.com/banner2.jpg'}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == 'http://banners.com/banner1.jpg'

        session = {'current_banner' : 'http://banners.com/banner1.jpg'}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == 'http://banners.com/banner2.jpg'

        session = {'current_banner' : 'http://banners.com/banner2.jpg'}
        updated, img = models.Img_limit.get_most_suitable_img(session,suitable_imgs)
        assert img == ''

if __name__ == '__main__':
    unittest.main()