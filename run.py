#!flask/bin/python
from app import app, db, models
from settings import HOST, PORT
from extra.read_csv import read_csv
from settings import csv_config_file

#Create db if needed
db.create_all()

#Read CSV configuration file. dict_img_limit: dict[img] = prepaid_show_amount
dict_img_limit = read_csv(csv_config_file)[1]

#Clear old data in Database
models.Img_limit.query.delete()
#Insert data parsed from CSV config file
for img in dict_img_limit:
    db.session.add(models.Img_limit(img_url = img, limit = dict_img_limit[img]))
db.session.commit()

app.run(HOST,PORT, debug = True)