from app import db
from sqlalchemy.sql import func

class Img_limit(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    img_url = db.Column(db.String(64), unique = True)
    limit = db.Column(db.Integer)

    def __repr__(self):
        return '<Img_limit %r>' % (self.img_url)

    @staticmethod
    def get_most_suitable_img(session,suitable_imgs):
        updated = 0 # if updated > 0: databases is changed
        img = ''
        # Choose img with the highest limit_show_amount if this image was not shown previously and in suitable_imgs
        if session.get('current_banner'): #for repeat avoiding
            subquery = db.session.query(Img_limit.limit).filter(Img_limit.img_url != session.get('current_banner')).\
                filter(Img_limit.img_url.in_(suitable_imgs)).subquery()
        else:
            subquery = db.session.query(Img_limit.limit).filter(Img_limit.img_url.in_(suitable_imgs)).subquery()
        query = db.session.query(Img_limit.img_url, Img_limit.limit).filter(Img_limit.img_url != session.get('current_banner')).\
            filter(Img_limit.limit == func.max(subquery.c.limit).select())
        img_limit = query.first()
        if img_limit:
            limit = img_limit[1]
            if limit > 0:
                updated = query.update({'limit':Img_limit.limit-1},synchronize_session = 'fetch')
                db.session.commit()
                img = img_limit[0]
        #What if current_banner is the only one suitable image
        if not updated and session.get('current_banner'):
            subquery = db.session.query(Img_limit.limit).filter(Img_limit.img_url.in_(suitable_imgs)).subquery()
            query = db.session.query(Img_limit.img_url, Img_limit.limit).filter(Img_limit.img_url != session.get('current_banner')).\
                filter(Img_limit.limit == func.max(subquery.c.limit).select())
            img_limit = query.first()
            if img_limit:
                limit = img_limit[1]
                if limit > 0:
                    updated = query.update({'limit':Img_limit.limit-1},synchronize_session = 'fetch')
                    db.session.commit()
                    img = img_limit[0]
        return updated, img