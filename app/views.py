from app import app, db
from flask import request, session, render_template
from models import Img_limit
from settings import csv_config_file
from extra.read_csv import read_csv

#Read CSV configuration file. dict_cat_img: dict[category] = [img, img, ...]; dict_img_limit: dict[img] = prepaid_show_amount
dict_cat_img, dict_img_limit = read_csv(csv_config_file)

@app.route('/')
def get_banner():
    # List of categories from get-request
    categories = [c.split('=')[1] for c in request.query_string.split('&') if 'category[]=' in c]

    if len(categories) > 10:
        return "Requested too many categories."

    # List of all suitable images
    if categories:
        suitable_imgs = set(sum([dict_cat_img[cat] for cat in categories if cat in dict_cat_img],[]))
    else:
        suitable_imgs = dict_img_limit.keys()

    # Get most suitable img
    updated, img = Img_limit.get_most_suitable_img(session,suitable_imgs)

    if updated:
        session['current_banner'] = img
        return render_template("banner.html",img = img)

    return 'Banners are not available'