Step by step instruction
========================

1. git clone git@bitbucket.org:AlexMurm/bannerokrutilka.git
2. cd bannerokrutilka
3. virtualenv venv
4. source venv/bin/activate
5. pip install -r requirements.txt
6. python run.py (run web-server)

In order to run tests: python tests/tests.py

Python version - 2.7