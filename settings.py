import os
basedir = os.path.abspath(os.path.dirname(__file__))
import socket

SECRET_KEY = 'super-secret'
csv_config_file = os.path.join(basedir,'config.csv')
db_name = os.path.join(basedir,'banner.db')

HOST = socket.gethostbyname(socket.gethostname())
PORT = 8080

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, db_name)